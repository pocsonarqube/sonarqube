/**
 * Created by JKESSEN on 22-5-2017.
 */
describe('DemoCtrl', function() {
    beforeEach(module('inputBasicDemo'));

    var $controller;

    beforeEach(inject(function(_$controller_){
        $controller = _$controller_;
    }));

    describe('$scope.user.title', function() {
        it('Sets the initial title to Developer', function() {
            var $scope = {};
            var controller = $controller('DemoCtrl', { $scope: $scope });
            expect($scope.user.title).toEqual('Developer');
        });
    });
});