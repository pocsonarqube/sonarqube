/**
 * Dummy gulpfile.js
 */
var gulp = require('gulp');

/**
 * Dummy build task
 */
gulp.task('build', function (done) {
    console.log('Building stuff ...');
    console.log('Done');
    done();
});

gulp.task('test', function (done) {
    console.log('Testing stuff ...');
    console.log('Done');
    done();
});
